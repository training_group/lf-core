import * as jsts from 'jsts'

function Intersection(geom1, geom2) {

  const parser = new jsts.io.GeoJSONReader();
  const parserForward = new jsts.io.GeoJSONWriter();

  try {
    const jstsGeom = parser.read(geom1.geometry || geom1);
    const jstsGeom2 = parser.read(geom2.geometry || geom2);
    const intersection = jstsGeom.intersection(jstsGeom2);

    return ({ data: parserForward.write(intersection), status: true })
  }
  catch (e) {
    return ({ data: {}, status: false })
  }

}

export default Intersection;