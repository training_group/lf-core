const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    geo: './core/index.js',
  },
  output: {
    path: path.resolve(__dirname, './assets'),
    filename: '[name].bundle.js',
    publicPath: '/assets',
    library: 'gisApi',
    libraryTarget: 'umd',
    crossOriginLoading: false,
  },
  module: {

    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ],
  },
};
