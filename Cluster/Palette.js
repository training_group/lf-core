import Color from "./Color";

class Palette {
  constructor(props) {
    this.baseColor = props.baseColor || { r: 0, g: 0, b: 0 };
    this.secondColor = props.secondColor || { r: 0, g: 0, b: 0 };
    this.thirdColor = props.thirdColor || { r: 0, g: 0, b: 0 };
  }


  static init(props) {
    const rotation = props ? props.rotation || 90 : 90;
    const baseColor = new Color();

    const secondColor = new Color(
      baseColor.changeHue(baseColor.hue, rotation),
      baseColor.sat,
      baseColor.changeLight(baseColor.light));

    const thirdColor = new Color(
      baseColor.changeHue(baseColor.hue, rotation + rotation),
      baseColor.sat,
      baseColor.changeLight(baseColor.light));
    return new Palette({
      baseColor: baseColor.rgb,
      secondColor: secondColor.rgb,
      thirdColor: thirdColor.rgb
    });
  }

}

export default Palette;
