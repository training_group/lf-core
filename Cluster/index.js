import { diagram } from "../Symbol";
import Palette from "./Palette";
import styles from "./cluster-styles.css";

class Cluster {
  constructor(map, L) {
    this.map = map;
    this.L = L;
    this.flag = false;
    this.clusters = {};

    this.init = this.init.bind(this);
    this.createMarkers = this.createMarkers.bind(this);
    this.drawDiagram = this.drawDiagram.bind(this);
    this.defaultIconCreateFunction = this.defaultIconCreateFunction.bind(this);
    this.createDataForSVG = this.createDataForSVG.bind(this);
    this.sizeIconCreateFunction = this.sizeIconCreateFunction.bind(this);
    this.remove = this.remove.bind(this);
  }

  static getCoordinates(feature) {
    const { geometry: { type, coordinates } } = feature;
    if (type === "Point") {
      const [x, y] = coordinates;
      return [y, x];
    } else {
      return [0, 0];
    }
  }

  static addProperties(marker, feature, attributes) {
    for (let i = 0; i < attributes.length; i++) {
      const attribute = attributes[i];
      marker[attribute] = feature.properties[attribute] || 0;
    }
    return marker;
  }

  static createStyleRules(className, size) {
    const width = size || 30;
    const height = size || 30;
    const borderRadius = size ? size / 2 : 15;

    const style = document.createElement('style');
    style.innerHTML = `
    .${className} div {
      width: ${width}px !important;
      height: ${height}px !important;
      border-radius: ${borderRadius}px
     }`;
    document.head.appendChild(style);
  }


  init(layerId, type, features, attributes) {
    const self = this;
    self.attributes = attributes;
    const instanceCluster = self.createMarkers(layerId, type, features, attributes);
  }

  remove(layerId) {
    const self = this;
    const markers = self.clusters[layerId];
    if (markers) {
      markers.remove()
    }
  }

  drawDiagram(cluster) {
    const self = this;
    const markers = cluster.getAllChildMarkers();
    const svg = diagram(self.createDataForSVG(markers), 40, 40);

    return new self.L.DivIcon({
      html: '<div>' + svg + '</div>',
      className: '',
      iconSize: new self.L.Point(40, 40)
    });
  }

  defaultIconCreateFunction(cluster) {
    const self = this;
    const childCount = cluster.getChildCount();

    const { baseColor, secondColor, thirdColor } = self.palette;

    let style = `background-color:rgba(${baseColor.r}, ${baseColor.g}, ${baseColor.b}, 0.6)`;
    if (childCount < 10) {
      style = `background-color:rgba(${baseColor.r}, ${baseColor.g}, ${baseColor.b}, 0.6)`;
    } else if (childCount < 100) {
      style = `background-color:rgba(${secondColor.r}, ${secondColor.g}, ${secondColor.b}, 0.6)`;
    } else {
      style = `background-color:rgba(${thirdColor.r}, ${thirdColor.g}, ${thirdColor.b}, 0.6)`;
    }

    if (!self.flag) {
      Cluster.createStyleRules('lf-core-marker-cluster');
    }

    self.flag = true;

    return new self.L.DivIcon({
      html: '<div style="' + style + '"><span>' + childCount + '</span></div>',
      className: 'lf-core-marker-cluster',
      iconSize: new self.L.Point(40, 40)
    });
  }

  sizeIconCreateFunction(cluster) {

    const self = this;
    const childCount = cluster.getChildCount();

    const { baseColor, secondColor, thirdColor } = self.palette;
    const size = 25 + Math.log(childCount) * 5;

    let style = `background-color:rgba(${baseColor.r}, ${baseColor.g}, ${baseColor.b}, 0.6)`;
    if (childCount < 10) {
      style = `background-color:rgba(${baseColor.r}, ${baseColor.g}, ${baseColor.b}, 0.6)`;
    } else if (childCount < 100) {
      style = `background-color:rgba(${secondColor.r}, ${secondColor.g}, ${secondColor.b}, 0.6)`;
    } else {
      style = `background-color:rgba(${thirdColor.r}, ${thirdColor.g}, ${thirdColor.b}, 0.6)`;
    }

    Cluster.createStyleRules('lf-core-marker-cluster', size);

    return new self.L.DivIcon({
      html: '<div style="' + style + '"><span>' + childCount + '</span></div>',
      className: 'lf-core-marker-cluster',
      iconSize: new self.L.Point(40, 40)
    });
  }

  createMarkers(layerId, type, features, attributes) {
    const self = this;
    const typeFunction = {
      "diagram": this.drawDiagram,
      "size": this.sizeIconCreateFunction,
      "default": this.defaultIconCreateFunction,
    };
    if (type !== "diagram") {
      self.palette = Palette.init();
    }

    const iconCreateFunction = typeFunction[type] || this.defaultIconCreateFunction;


    const markers = self.L.markerClusterGroup(
      {
        iconCreateFunction,
      }
    );

    for (let i = 0; i < features.length; i++) {
      const feature = features[i];
      const m = self.L.marker(Cluster.getCoordinates(feature), { title: i });
      markers.addLayer(Cluster.addProperties(m, feature, attributes));
    }
    self.clusters[layerId] = markers;
    self.map.addLayer(markers);
  }

  static getValueAttribute(markers, attribute) {
    let value = 0;
    for (let i = 0; i < markers.length; i++) {
      const val = markers[i][attribute] || 0;
      value = value + val;
    }
    return value;
  }

  createDataForSVG(markers) {
    const self = this;
    const { attributes } = self;
    return attributes.map((attribute) => {
      return {
        name: attribute,
        value: Cluster.getValueAttribute(markers, attribute)
      }
    });
  }
}

export default Cluster;
