class Color {
  static randomNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  static hsvToRGB(h, s, v) {
    const c = v * s;
    const x = c * (1 - Math.abs((h / 60) % 2 - 1));
    const m = v - c;

    let R, G, B;
    if (h >= 0 && h < 60) {
      R = c;
      G = x;
      B = 0;
    } else if (h >= 60 && h < 120) {
      R = x;
      G = c;
      B = 0;
    } else if (h >= 120 && h < 180) {
      R = 0;
      G = c;
      B = x;
    } else if (h >= 180 && h < 240) {
      R = 0;
      G = x;
      B = c;
    } else if (h >= 240 && h < 300) {
      R = x;
      G = 0;
      B = c;
    } else if (h >= 300 && h < 360) {
      R = c;
      G = 0;
      B = x;
    }
    return {
      r: (R + m) * 255,
      g: (G + m) * 255,
      b: (B + m) * 255,
    }
  }

  constructor(hue, sat, light) {
    this.minHue = 0;
    this.maxHue = 360;

    this.minSat = 75;
    this.maxSat = 100;

    this.minLight = 40;
    this.maxLight = 65;

    this.scaleLight = 15;
    this.hue = hue || Color.randomNum(this.minHue, this.maxHue);

    if (this.hue > 288 && this.hue < 316) {
      this.hue = Color.randomNum(316, 360);
    } else if (this.hue > 280 && this.hue < 288) {
      this.hue = Color.randomNum(260, 280);
    }

    this.sat = sat || Color.randomNum(this.minSat, this.maxSat);
    this.light = light || Color.randomNum(this.minLight, this.maxLight);

    this.rgb = Color.hsvToRGB(this.hue, this.sat, this.light);
    this.changeHue = this.changeHue.bind(this);
    this.changeLight = this.changeLight.bind(this);
  }

  changeHue(hue, rotate) {
    return hue + rotate > this.maxHue ?
      (hue + rotate) - this.maxHue : hue + rotate;
  };

  changeLight(light) {
    return light + this.scaleLight > this.maxLight ? this.maxLight : light + this.scaleLight;
  };
}

export default Color;
