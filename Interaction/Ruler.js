class Ruler {
  constructor(map, L) {
    this.map = map;
    this.L = L;
    this.on = this.on.bind(this);
    this.off = this.off.bind(this);
  }

  on() {
    const self = this;
    self.L.control.ruler().addTo(self.map);
  };

  off() {
    const self = this;

  }
}

export default Ruler;