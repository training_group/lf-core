class Drag {
  constructor(map) {
    this.map = map;
    this.on = this.on.bind(this);
    this.off = this.off.bind(this);
  }

  on(layerGroup) {
    const self = this;
    const layers = layerGroup.getLayers();
    for (let i = 0; i < layers.length; i++) {
      const layer = layers[i];
      layer.dragging.enable()
    }
  };

  off(layerGroup) {
    const self = this;
    const layers = layerGroup.getLayers();
    for (let i = 0; i < layers.length; i++) {
      const layer = layers[i];
      layer.dragging.disable()
    }
  }
}

export default Drag;
