class Select {
  constructor(map, layer, L) {
    this.map = map;
    this.L = L;
    this.layer = layer;
    this.init = this.init.bind(this);
    this.off = this.off.bind(this);

    this.defaultStyle = {
      color: "#3388ff",
      weight: 3,
      opacity: 1,
      fillColor: null,
      fillOpacity: 0.2
    };

    this.selectStyle = {
      color: "rgba(205,207,0,0.83)",
      weight: 5,
      opacity: 1,
      fillColor: null,
      fillOpacity: 0.2
    };

    this.selectIcon = {
      iconUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAABl5JREFUWIWll3tsW1cdx7/n3Hude23nVcc0aVgfPNatpVmULXTd1I6OiUerCSa0iAkmhBgaqBtIqKqophLM1DI2EAjaQbXuUSFUqRtUgWkVKtK2bq3okiVp0kVloiEPJ67T2Int6/s4955z+CNpWsdO7GzfP8/v8Tnf370+x5egQrUfOhOhrtcsVdlAfDKtBZB496ld1yqpJcsF7/rF6Y0qwXeJxLclZDOl1FYoFVwIKoQwCEgSwAnB5csXYrsGVgbp7KRb6dZ9hCJWHQrLUDhYFTQMaApdSOGCw3IYTDPPsrkchBC/ek90/xyxmCgLuavz9U+pBCe1QOC2psbVIVVTwbiAzyVOjbcs5D10ywBUhSCgUAjuYWJyKs8YG+ZUebjnZ1/9z5KQ9gOnvkwo7aqvq6PRaESzmMRfx7YsPc95fWPtIIIBglRqxk+l01wK+Uj3018/VQRpP3QmQpz8lTVrmmt1I4iM4+ONxB1lATfrkQ0fwHFtTMbjeeorG/996MEJALgxZMs8Xh0O67puIGWyFQMA4MT/NkMP6KiprlU59f9c4KT9qZPfUhTt2LoNn9EzjsDpZCFgV9NFhAMqFApIIUAoBRfAydHNJWHfXDeIsZErtu97P+4++PAL5M7OvweJx6bXfHKtAc1A10ThM/ha8yB0RSCbvoacaYJ5PgKainAwiOpVnwCHgr/Fi2vg2ZiMj9lSCzRQOE6bohBUGaEiwO7GAVDuYGLkv9i2vgavPP4FnIs9hL/suR9f2hRFYuwKOLPxYFPhT6RrYguqjBAUhQCO06YCfruuh+D6ha/3/ZE+RDSCVCKB739xE76z47aF2LqGavxkdys2NtXh2df70HDLZ4tG5voCuq7DZJl2CpAdWlXIYIsgqkpgmVnUBige3b6x5Ox3t63H+lUh5GfTRTHmC2hVIQMgO6iU8u4qPVj0sFVJ4boO2j8dBSVLnz7bbm2E79rY3Vg4stPJO1ClByGlvJsK4QeX7MA9hHR1yTAAGAEFED4gS8eF4AYlkvS5joUHIv2FdrkA0XT0XkkuC+kdTkIqOjxeSHkg0g/XsUAk+imX/jtuPueptHAktiegB2vxYWIGZ4fGSwIGRq7hwocJGOF6OJwXxFRK4Fh5JuC/RRXOu10rx5RFkHPZNticIBxpxr7jb+G185fh8bmXQ0iJf/YN44dHzyAcaQKDirMzbQX1CiVwc7MMnHerrue8JyENiMKdAMC/Uq34SvQiahoD+O0/enHotXNorK/B1KwJVVURjm5AIFSLN6ZaimohOBizDM9z3ycA0PKjFy/UrV77eRqK4s3Z1qL8+2r6EdIphOfCZw7UgA6qVcF2Bd7MFOfvrOuHMK/J2amx7oHff2+rCgBS+L/OpiaON9auNkrN/u1sK5C9aSFfKuuGdJUiMR13CNgzwPwpzCP1XZw5rmNlcE+4d/kOZXRPuBeOlQHnLHex/mrXAmQo1sGIlM+bs0nb0JSPBQkFFORScZsK8dz1q3jhPvEl/wPLTlPhWh8LwiwTXj5DGfyXr68tQIaef+KqlPJwLjVi76ztL92hjHbW9sNMjdiQ8jeXjzyZKoIAgAf/l14+Q33H/EgQ3zHhWTkRtrVnbl4vgFw+8mSKSPlsPjVq31ezcjfm9LBNIA+ef+mx3JIQAAjZ2nO+nRO+nV0cWlb3amfBXcsL29rhxbEiyPmXHssRyINWesTeHuqrCLA91AcrNWwTKZ9e7KIkBADSU5HfcddyPWu2LOBO7X141iw4c+z0VORIqZySkPirHTaB6HQzo/a9weXd6BqFMzNiA/JA/NUOu2IIAPiRhj/5npvzrOKr9bpalR54VhqcsxyPRI4tlbckZCjWwYjgB1h61N6mlz5qDI2CpUdtCr5/KNbBVgwBgMGG5DEh2AzLp7AFPQWxFtIDlk9BCG96YFXyleX6LAtBLCakkPt4ZtQxtMJUXaXgmVFHCrG/1OdC5RAAl6JXTwjBJ31rCpvlnJvNsgd+Pgkh+OSl6NUT5XqUhczv8qciF3eM+T8uhgoIc9ylVOwt5wIo8zl3Q5JseeLoB2rNutsRXA2ZT0ieG788ePgHmyqpLu9kbi9SCrlXmOOu9BlkfoJJIfdWVluxkzl9bs/RXqpqrcL3+i8debytfMWcKnQyn0z4XnCXUMIrdvGR1LLnj/tXWvN/KC0PBVywcdUAAAAASUVORK5CYII=",
      iconRetinaUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAABl5JREFUWIWll3tsW1cdx7/n3Hude23nVcc0aVgfPNatpVmULXTd1I6OiUerCSa0iAkmhBgaqBtIqKqophLM1DI2EAjaQbXuUSFUqRtUgWkVKtK2bq3okiVp0kVloiEPJ67T2Int6/s4955z+CNpWsdO7GzfP8/v8Tnf370+x5egQrUfOhOhrtcsVdlAfDKtBZB496ld1yqpJcsF7/rF6Y0qwXeJxLclZDOl1FYoFVwIKoQwCEgSwAnB5csXYrsGVgbp7KRb6dZ9hCJWHQrLUDhYFTQMaApdSOGCw3IYTDPPsrkchBC/ek90/xyxmCgLuavz9U+pBCe1QOC2psbVIVVTwbiAzyVOjbcs5D10ywBUhSCgUAjuYWJyKs8YG+ZUebjnZ1/9z5KQ9gOnvkwo7aqvq6PRaESzmMRfx7YsPc95fWPtIIIBglRqxk+l01wK+Uj3018/VQRpP3QmQpz8lTVrmmt1I4iM4+ONxB1lATfrkQ0fwHFtTMbjeeorG/996MEJALgxZMs8Xh0O67puIGWyFQMA4MT/NkMP6KiprlU59f9c4KT9qZPfUhTt2LoNn9EzjsDpZCFgV9NFhAMqFApIIUAoBRfAydHNJWHfXDeIsZErtu97P+4++PAL5M7OvweJx6bXfHKtAc1A10ThM/ha8yB0RSCbvoacaYJ5PgKainAwiOpVnwCHgr/Fi2vg2ZiMj9lSCzRQOE6bohBUGaEiwO7GAVDuYGLkv9i2vgavPP4FnIs9hL/suR9f2hRFYuwKOLPxYFPhT6RrYguqjBAUhQCO06YCfruuh+D6ha/3/ZE+RDSCVCKB739xE76z47aF2LqGavxkdys2NtXh2df70HDLZ4tG5voCuq7DZJl2CpAdWlXIYIsgqkpgmVnUBige3b6x5Ox3t63H+lUh5GfTRTHmC2hVIQMgO6iU8u4qPVj0sFVJ4boO2j8dBSVLnz7bbm2E79rY3Vg4stPJO1ClByGlvJsK4QeX7MA9hHR1yTAAGAEFED4gS8eF4AYlkvS5joUHIv2FdrkA0XT0XkkuC+kdTkIqOjxeSHkg0g/XsUAk+imX/jtuPueptHAktiegB2vxYWIGZ4fGSwIGRq7hwocJGOF6OJwXxFRK4Fh5JuC/RRXOu10rx5RFkHPZNticIBxpxr7jb+G185fh8bmXQ0iJf/YN44dHzyAcaQKDirMzbQX1CiVwc7MMnHerrue8JyENiMKdAMC/Uq34SvQiahoD+O0/enHotXNorK/B1KwJVVURjm5AIFSLN6ZaimohOBizDM9z3ycA0PKjFy/UrV77eRqK4s3Z1qL8+2r6EdIphOfCZw7UgA6qVcF2Bd7MFOfvrOuHMK/J2amx7oHff2+rCgBS+L/OpiaON9auNkrN/u1sK5C9aSFfKuuGdJUiMR13CNgzwPwpzCP1XZw5rmNlcE+4d/kOZXRPuBeOlQHnLHex/mrXAmQo1sGIlM+bs0nb0JSPBQkFFORScZsK8dz1q3jhPvEl/wPLTlPhWh8LwiwTXj5DGfyXr68tQIaef+KqlPJwLjVi76ztL92hjHbW9sNMjdiQ8jeXjzyZKoIAgAf/l14+Q33H/EgQ3zHhWTkRtrVnbl4vgFw+8mSKSPlsPjVq31ezcjfm9LBNIA+ef+mx3JIQAAjZ2nO+nRO+nV0cWlb3amfBXcsL29rhxbEiyPmXHssRyINWesTeHuqrCLA91AcrNWwTKZ9e7KIkBADSU5HfcddyPWu2LOBO7X141iw4c+z0VORIqZySkPirHTaB6HQzo/a9weXd6BqFMzNiA/JA/NUOu2IIAPiRhj/5npvzrOKr9bpalR54VhqcsxyPRI4tlbckZCjWwYjgB1h61N6mlz5qDI2CpUdtCr5/KNbBVgwBgMGG5DEh2AzLp7AFPQWxFtIDlk9BCG96YFXyleX6LAtBLCakkPt4ZtQxtMJUXaXgmVFHCrG/1OdC5RAAl6JXTwjBJ31rCpvlnJvNsgd+Pgkh+OSl6NUT5XqUhczv8qciF3eM+T8uhgoIc9ylVOwt5wIo8zl3Q5JseeLoB2rNutsRXA2ZT0ieG788ePgHmyqpLu9kbi9SCrlXmOOu9BlkfoJJIfdWVluxkzl9bs/RXqpqrcL3+i8debytfMWcKnQyn0z4XnCXUMIrdvGR1LLnj/tXWvN/KC0PBVywcdUAAAAASUVORK5CYII=",
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      tooltipAnchor: [16, -28],
      shadowSize: [41, 41],
    };
  }

  init(func) {
    const self = this;
    for (let item in self.layer.layers) {
      const layer = self.layer.layers[item];
      layer.on('click', function (e) {
        if (func) {
          func(e)
        }
        if (self.feature) {
          if (self.feature.setStyle) {
            self.feature.setStyle(self.defaultStyle)
          }

          if (self.feature.setIcon) {
            const defaultIcon = new self.L.Icon.Default();
            self.feature.setIcon(defaultIcon)
          }
        }
        self.feature = e.sourceTarget;
        if (e.sourceTarget.setStyle) {
          e.sourceTarget.setStyle(self.selectStyle)
        }

        if (e.sourceTarget.setIcon) {
          const selectIcon = self.L.icon(self.selectIcon);
          e.sourceTarget.setIcon(selectIcon);
        }

      });
    }

    self.map.on('click', function (e) {
      if (self.feature && e.originalEvent.target.localName !== 'path') {
        if (self.feature.setStyle) {
          self.feature.setStyle(self.defaultStyle)
        }

        if (self.feature.setIcon) {
          const defaultIcon = new self.L.Icon.Default();
          self.feature.setIcon(defaultIcon)
        }
      }
    })
  }

  off() {
    const self = this;

    if (!self.feature) {
      return null;
    }

    if (self.feature.setStyle) {
      self.feature.setStyle(self.defaultStyle)
    }

    if (self.feature.setIcon) {
      const defaultIcon = new self.L.Icon.Default();
      self.feature.setIcon(defaultIcon)
    }

    for (let item in self.layer.layers) {
      const layer = self.layer.layers[item];
      layer.off('click');
      self.map.off('click');
    }
  }
}

export default Select;