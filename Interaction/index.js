import Draw from './Draw'
import Modify from "./Modify";
import Drag from "./Drag";
import Select from "./Select";
import Intersection from './Intersection';
import Ruler from './Ruler';

class Interaction {
  constructor(map, layer, L, utils) {
    this.map = map;
    this.draw = new Draw(map, L);
    this.modify = new Modify(map);
    this.drag = new Drag(map);
    this.select = new Select(map, layer, L);
    this.intersection = new Intersection(map, layer, L, utils);
    this.ruler = new Ruler(map, L);
  }
}

export default Interaction;
