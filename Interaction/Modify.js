class Modify {
  constructor(map) {
    this.map = map;
    this.on = this.on.bind(this);
    this.off = this.off.bind(this);
  }

  on(layerGroup) {
    const self = this;
    const layers = layerGroup.getLayers();
    for (let i = 0; i < layers.length; i++) {
      const layer = layers[i];
      layer.editing.enable()
    }
  };

  off(layerGroup) {
    const self = this;
    const layers = layerGroup.getLayers();
    for (let i = 0; i < layers.length; i++) {
      const layer = layers[i];
      layer.editing.disable()
    }
  }
}

export default Modify;
