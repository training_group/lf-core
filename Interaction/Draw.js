class Draw {
  constructor(map, L) {
    this.map = map;
    this.L = L;
    this.init = this.init.bind(this);
    this.stop = this.stop.bind(this);
  }

  init(type, layer, func) {
    const self = this;
    self.isReturn = false;
    let feature;
    switch (type) {
      case 'Polyline': {
        feature = self.map.editTools.startPolyline();
        break;
      }
      case 'Polygon': {
        feature = self.map.editTools.startPolygon();
        break;
      }
      case 'Marker': {
        feature = self.map.editTools.startMarker();
        break;
      }
      case 'Rectangle': {
        feature = self.map.editTools.startRectangle();
        break;
      }
      case 'Circle': {
        feature = self.map.editTools.startCircle();
        break;
      }
    }

    feature.on('editable:drawing:end', function (e) {
      if (!self.isReturn) {
        self.isReturn = true;
        e.target.disableEdit();
        const json = e.target.toGeoJSON();
        layer.addData(json);
        e.target.remove();
        if (func) func(json);
      }
    });

    feature.on('editable:drawing:cancel', function (e) {
      if (!self.isReturn) {
        self.isReturn = true;
        const json = e.target.toGeoJSON();
        layer.addData(json);
        e.target.remove();
        if (func) func(json);
        feature.off('editable:drawing:cancel');
      }
    });
  }

  stop() {
    const self = this;
    self.map.editTools.stopDrawing();
  }
}

export default Draw;
