class Intersection {
  constructor(map, layer, L, utils) {
    this.map = map;
    this.utils = utils;
    this.layer = layer;
    this.L = L;
    this.init = this.init.bind(this);
    this.off = this.off.bind(this);
    this.geoms = [];
    this.layerInt = null;

    this.interStyle = {
      color: "rgba(255,0,6,0.83)",
      weight: 5,
      opacity: 1,
      fillColor: null,
      fillOpacity: 0.2
    };
  }

  init(callback) {
    const self = this;
    if (self.layerInt) {
      self.layerInt.remove();
      self.layerInt = null;
    }
    for (let item in self.layer.layers) {
      const layer = self.layer.layers[item];

      layer.on('click', function (e) {
        if (e.sourceTarget.feature) {
          self.geoms.push(e.sourceTarget.toGeoJSON());
        }
        if (self.geoms.length === 2) {
          const inter = self.utils.Intersection(self.geoms[0], self.geoms[1]);
          if (inter.status) {
            //let feature = layer.addData(inter.data);
            //feature.setStyle(self.interStyle);

            const layer = self.L.geoJSON(inter.data, { style: self.interStyle }).addTo(self.map);
            self.layerInt = layer;
          }
          self.geoms = [];
          layer.off('click');
          if (callback){ callback();}
        }
      })
    }
  }

  off() {
    const self = this;

    if (self.layerInt) {
      self.layerInt.remove();
      self.layerInt = null;
    }
    self.geoms = [];
    for (let item in self.layer.layers) {
      const layer = self.layer.layers[item];
      layer.off('click')
    }
  }
}

export default Intersection;