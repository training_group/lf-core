import Map from "../Map";
import Layer from "../Layer";
import Interaction from '../Interaction';
import Cluster from '../Cluster';
//import Features from '../Features';

import Utils from '../Utils/index.js'


class Core {
  constructor(target) {
    this.utils = Utils;
    this.initMap(target);
    this.initLayer();
    this.initInteraction();
    //this.initFeatures();

    this.initCluster();
  }

  initMap(target) {
    let self = this;
    const _map = new Map();
    const { map: newMap, L } = _map.create(target);
    self.map = newMap;
    self._map = _map;
    self.L = L;
  }

  initLayer() {
    let self = this;
    self.layer = new Layer(self.map, self.L)
  }

  initInteraction() {
    let self = this;
    self.interaction = new Interaction(self.map, self.layer, self.L, self.utils)
  }

  initFeatures() {
    let self = this;
    self.features = new Features()
  }

  initCluster(){
    let self = this;
    self.cluster = new Cluster(self.map, self.L)
  }
}

export { Core };
