# API lf-core

## Оглавление

1. [Core](#core)
2. [Map](#map)
3. [Layer](#layer)
4. [Interaction](#interaction)
    1. [List](#list)
5. [Utils](#utils)


## Core

Для работы с API ol-core необходимо подключить файл со сборкой к Вашему проекту.  
**Пример**  
```
<script src="../assets/geo.bundle.js"></script>
<script>
     let core = new gisApi.Core('map');
</script>
```
Вызов
```javascript
new gisApi.Core([target]);
```

* `target` - id div куда вставлять карту. Поумолчанию 'map'

Return - ``{map, layer, interaction, utils}``

## Map
***map.addLayer(layer)*** 

Добавление слоя на карту с помощью встроенной функции leaflet

* ``layer`` - Слой полученный при [создании слоя](#layer)

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.map.addLayer(layer);
```

____

***_map.setBaseMap(options)***

Изменение подложки

* ``ooptions{}`` -  ``url``: ссылка на подложку; ``maxZoom``: максимально приближение; ``minZoom``: минимально приближение

Return - ``layer``

Пример:

```
const core = new gisApi.Core();
core._map.setBaseMap({url:'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', maxZoom: 10, minZoom:3})
```

____

## Layer
***layer.createGeoJson(nameLayer)***

Создание слоя с GeoJson форматом

* ``nameLayer`` - Название слоя. При отсутствие названия он будет создан с `default` названием. Если название совподает, то произойдет перезапись слоя.

Return - ``layer``

Пример:

```
const core = new gisApi.Core();
let layer = core.layer.createGeoJson('test');
```

***layer.addGeoJson(nameLayer, geoJsonObject)***

Создание слоя с GeoJson форматом

* ``nameLayer`` - Название слоя. При отсутствие названия он будет создан с `default` названием. Если название совподает, то произойдет перезапись слоя.
* ``geoJsonObject`` - GeoJson объект

Return - ``layer``

Пример:

```
const core = new gisApi.Core();
let layer = core.layer.createGeoJson('test');

let geoJsonObject = {
      "type": "Feature",
      "properties": {
        "name": "Coors Field",
        "amenity": "Baseball Stadium",
        "popupContent": "This is where the Rockies play!"
      },
      "geometry": {
        "type": "Polygon",
        'coordinates': [[[10, 10], [-20, 20], [-30, -30], [-40, 40]]]
      }
    };
    
layer = core.layer.addGeoJson('test', geojsonObject);
```

____


***layer.remove(nameLayer)***

Удаление слоя

* ``nameLayer`` - Название слоя.

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.layer.remove('test');
```

____


## Interaction

***interaction.modify.on(layer)***

Редактирование фич на слое

* ``layer`` - Слой для редактирования

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.interaction.modify.on(layer);
```

____

***interaction.modify.off(layer)***

Отклбчние Редактирования фич


* ``layer`` - Слой

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.interaction.modify.off(layer);
```

____

***interaction.drag.on(layer)***

Перемещение фич

* ``layer`` - Слой для редактирования

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.interaction.drag.on(layer);
```

____

***interaction.drag.off(layer)***

Отключние перемещения фич


* ``layer`` - Слой

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.interaction.drag.off(layer);
```

____

***interaction.select.init(func)***

Выделение фичи

* ``func`` - Функция, которая будет вызвана при выделении фичи

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.interaction.select.init(func);
```

____

***interaction.select.off()***

Отключние выделения фичи

Return - ``Null``

Пример:

```
const core = new gisApi.Core();
core.interaction.select.off(layer);
```

____