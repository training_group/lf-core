import * as d3 from "d3";

function diagram(data, w, h) {
  const width = w || 60;
  const height = h || 60;
  const pie = d3.pie()
  .sort(null)
  .value(d => d.value);
  const arcs = pie(data);
  const arc = d3.arc()
  .innerRadius(0)
  .outerRadius(Math.min(width, height) / 2 - 1);

  const svg = d3.create("svg")
  .attr("width", width)
  .attr("height", height)
  .attr("viewBox", [-width / 2, -height / 2, width, height]);

  const arcLabel = () => {
    const radius = Math.min(width, height) / 2 * 0.5;
    return d3.arc().innerRadius(radius).outerRadius(radius);
  };

  const color = d3.scaleOrdinal()
  .domain(data.map(d => d.name))
  .range(d3.quantize(t => d3.interpolateSpectral(t * 0.8 + 0.1), data.length).reverse());


  svg.append("g")
  .attr("stroke", "white")
  .selectAll("path")
  .data(arcs)
  .join("path")
  .attr("fill", d => color(d.data.name))
  .attr("d", arc);

  svg.append("g")
  .attr("font-family", "sans-serif")
  .attr("font-size", Math.min(width, height) * 0.2)
  .attr("text-anchor", "middle")
  .selectAll("text")
  .data(arcs)
  .join("text")
  .attr("transform", d => `translate(${arcLabel().centroid(d)})`)
  .call(text => text.filter(d => (d.endAngle - d.startAngle) > 0.25).append("tspan")
  .attr("x", 0)
  .attr("y", 0)
  .attr("fill-opacity", 0.7)
  .text(d => Math.ceil(Number(d.data.value))));

  const node = svg.node();
  return node.outerHTML;
}

export default diagram;
