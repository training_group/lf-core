import L from 'leaflet';
import 'leaflet-draw';
import 'leaflet.path.drag';
import 'leaflet-editable';
import "leaflet.markercluster";
import "../Utils/Ruler";

class Map {
  constructor() {
    this.L = L;
    this.create = this.create.bind(this);
    this.setBaseMap = this.setBaseMap.bind(this);
  }

  create(target) {
    let self = this;
    const map = self.L.map(target, { editable: true }).setView([0, 0], 2);
    const baseMap = self.L.tileLayer(
      'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {
        attribution: '',
        maxZoom: 18,
      }).addTo(map);
    self.baseMap = baseMap;
    self.map = map;

    return { map, L: self.L };
  }

  setBaseMap({url, maxZoom, minZoom}){
    let self = this;
    self.baseMap.remove();
    const baseMap = self.L.tileLayer(
      url,
      {
        attribution: '',
        maxZoom: maxZoom || 18,
        minZoom: minZoom || 0,
      }).addTo(self.map);
    self.baseMap = baseMap;
    return baseMap;
  }
}

export default Map;
